;;; loader.el --- Organize custom elisp loading

;; Copyright (c) 2010 - 2020 Sebastian Rose, sebastian_rose gmx de
;; Authors:    Sebastian Rose, sebastian_rose gmx de

;; Released under the GNU General Public License version 3
;; see: http://www.gnu.org/licenses/gpl-3.0.html

;; This file is NOT part of GNU Emacs.

;; Released under the GNU General Public License version 3
;; see: http://www.gnu.org/licenses/gpl-3.0.html

;;; Commentary:

;; Trying to make heavy emacs customization bearable.

;; Adds a customization-group `user-login-name' if not exists.
;; Loads libraries from an easy to maintain list of symbols.
;; Read keybindings from a file.
;; Provides a command to check and report the keybindings for user-defined
;; commands.  This check can be forced after load, i.e. on emacs startup.


;; Installation
;; ------------

;; Make sure `loader.el' is in your `load-path' and add this to your emacs
;; setup (suppose "~/.emacs.d/custom/lisp" is where your custom elisp files are
;; found and loader.el is in there):

;;     (add-to-list 'load-path (concat "~/.emacs.d/custom/lisp"))
;;     (require 'loader)
;;     (loader-load)

;; Create a file, that holds the symbols you want to load on startup.  No need
;; to quote the symbols.  The loader will take care of that.
;; Here is an example:

;;     (
;;        my-first-module
;;        second-module-to-load
;;        my-cc
;;        my-flycheck
;;        my-gnus
;;        my-org-mode
;;        my-org-projects
;;        my-org-osm-maps
;;        my-perl
;;        ;; ...
;;        some-third-party-not-in-elpa
;;     )

;; If you like, put all your key-bindings for custom commands in a single file
;; and tell the loader to load it (see "Customization" below).  You could as
;; well add it to the end of the modules list.  But leaving it up to the loader
;; ensures, that the bindings are loaded after the command definitions.

;; Customization
;; -------------

;;  M-x customize-group RET loader RET

;; Customize everything as needed.  As a minimum, adjust
;; `loader-user-lisp-directory', `loader-user-lisp-filename-prefix',
;; `loader-user-lisp-command-regexp', `loader-module-file-name' and
;; `loader-kbd-config-file-name'.  All of which are fairly well documented.

;; Commands
;; --------

;; `loader-load' - Load all your custom modules.  This is done on start up.
;; `loader-check-key-bindings' - Check (custom) key bindings in current
;;         major mode and display a list in a '*help*' buffer.

;; `loader-add-module' - Add a module in `loader-user-lisp-directory' to
;;         your loader-modules.
;; `loader-remove-module' - Remove a module in from your loader-modules.
;; `loader-find-file' - Open a file in `loader-user-lisp-directory' for editing.

;; `loader-store-package-list' - Store a list of [m]elpa installed packages.
;; `loader-install-from-package-list' - Install the list of packages (if
;;         exists).


;;; Code:

(require 'cl-seq)                       ;; 'cl-remove-if'

(make-symbol user-login-name)

(defgroup loader nil
  "Custom module organizer and loader."
  :tag "Loader"
  :group (intern (user-login-name)))

(defcustom loader-user-lisp-directory (concat "~/.emacs.d/custom/lisp")
  "Directory with user-defined lisp packages."
  :group 'loader
  :type 'directory)

(defcustom loader-check-bindings-on-load nil
  "Should the loader check key bindings after load?
If non nil, check all commands found in files in `loader-user-lisp-directory'
whos names start with `loader-user-lisp-command-regexp' for a valid key binding.
The check can be done on demand using `loader-check-key-bindings'"
  :group 'loader
  :type 'boolean)

(defcustom loader-show-basename-in-keybindings-buffer t
  "Display file basenames instead of full paths in help buffer."
  :group 'loader
  :type 'boolean)

(defcustom loader-user-lisp-filename-prefix ""
  "Make `loader-check-bindings-on-load' scan only those files whose names sart
with this string.  Default is the empty string (scann all elisp files).  The
prefix is made into a regexp with `loader-user-lisp-directory' prepended and
[^/]+.elc?$ appended."
  :group 'loader
  :type 'regexp)

(defcustom loader-user-lisp-command-regexp ""
  "Require bindings for user defined commands who's names start with this prefix.
Set to an empty string if all commands loaded from `loader-user-lisp-directory'
shall require a key-binding.
See description for the variable `loader-check-bindings-on-load' and the
command `loader-check-key-bindings'."
  :group 'loader
  :type 'regexp)

(defcustom loader-module-file-name (concat loader-user-lisp-directory "/loader-modules.el")
  "Name of the file that is maintained bei the loader.
The file will contain all the names of the modules, you want to
load on startup."
  :group 'loader
  :type 'regexp)

(defcustom loader-kbd-config-file-name (concat loader-user-lisp-directory "/loader-kbd.el")
  "Name of the file that holds your key bindings."
  :group 'loader
  :type 'regexp)

(defcustom loader-stop-on-error-loading-modules t
  "Should the loader stop if the loading of modules failed?"
  :group 'loader
  :type 'boolean)

(defcustom loader-stop-on-error-loading-bindings t
  "Should the loader stop if the loading of key bindings failed?"
  :group 'loader
  :type 'boolean)



(defun file-string (file)
  "Read the contents of a file and return as a string."
  (with-temp-buffer
    (insert-file-contents file)
    (buffer-string)))

(defun loader-load ()
  "Load all configured modules."
  (interactive)
  (let ((lfn (file-truename loader-module-file-name))
        (kbd-config (file-truename loader-kbd-config-file-name))
        modules)

    (when (not (member loader-user-lisp-directory load-path))
      (add-to-list 'load-path loader-user-lisp-directory))

     ;; create an empty module file if necessary:
    (when (not (file-exists-p lfn))
      (with-temp-file lfn
        (insert "()\n")))

    ;; Load the modules
    (if (and (file-exists-p lfn)
             (file-readable-p lfn))
        (progn
          (setq modules (read (file-string lfn)))
          (message "= Loading Features from %s" lfn)
          (dolist (feature modules)
            (if (not (require feature nil t))
                (progn
                  (message "  [ ] %-27s" feature)
                  (error
		           "FAILED to load %s (check your %s and you load-path)"
		           feature lfn))
              (message "  [X] %-27s" feature)))
          (message "= Loading Features DONE"))
      (when loader-stop-on-error-loading-modules
        (error "Loaders module file does not exist yet: %s" lfn)))

    ;; Load key bindings
    (when (and (file-exists-p kbd-config)
               (file-readable-p kbd-config))
      (message "= Loading key bindings from %s" kbd-config)
        (when (not (load-file kbd-config))
          (if loader-stop-on-error-loading-bindings
              (error "Loaders keyboard config file not readable: %s" kbd-config)
            (message "= Loading key bindings from %s FAILED" kbd-config))))))

(defun loader-check-key-bindings ()
  "Search all loaded modules in `loader-user-lisp-directory' whos names start
with `loader-user-lisp-filename-prefix' for commands whos names start with
`loader-user-lisp-command-regexp'.  Lookup the key-bindings for all those
commands.  Commands and their bindings will be printed to the *Messages* buffer
for review."
  (interactive)

  (unless (and
           (stringp loader-user-lisp-directory)
           (file-directory-p loader-user-lisp-directory))
    (error "Not a directory: %s" loader-user-lisp-directory))

  (help-setup-xref (list 'describe-symbols loader-user-lisp-command-regexp) (interactive-p))

  (with-help-window (help-buffer) ;; "*key-bindings*"
      (let ((missing-bindings '())
        (loader-user-lisp-directory
         (directory-file-name
          (expand-file-name loader-user-lisp-directory)))
        (msg-format "  %-16s  %-35s  %s\n"))
      (princ (format "CUSTOM KEY BINDINGS in %s\n\n" major-mode))
      (princ (format msg-format "BINDING" "DEFUN" "FILENAME"))

      (dolist (f load-history)
        ;; TODO: Rely on loader-modules.el?
        (when (and
               (stringp (car f))
               (string-match (concat "^" loader-user-lisp-directory
                                     "/" loader-user-lisp-filename-prefix
                                     "[^/]+.elc?$")
                             (car f)))
          (let ((fname (car f))
                (symlist (cdr f)))
            (dolist (l symlist)
              (when (and
                     (listp l)
                     (equal 'defun (car l))
                     (commandp (cdr l)))

                (let* ((funcsym (cdr l))
                       (funcname (symbol-name funcsym)))
                  (when (string-match loader-user-lisp-command-regexp funcname)
                    (if (where-is-internal funcsym overriding-local-map)
                        (princ (format msg-format
                                (substitute-command-keys
                                 (concat "\\[" funcname "]"))
                                funcname
                                (if loader-show-basename-in-keybindings-buffer
                                    (file-name-nondirectory fname)
                                  fname)))
                      (setq
                       missing-bindings
                       (append
                        (list (format msg-format "UNBOUND:" funcname
                                      (if loader-show-basename-in-keybindings-buffer
                                          (file-name-nondirectory fname)
                                        fname)))
                        missing-bindings))))))))))

      (when (> (length missing-bindings) 0)
        (princ (format "\n\nNOT BOUND in %s mode\n\n" major-mode))
        (princ (format msg-format " " "DEFUN" "FILENAME"))
        (mapc 'princ missing-bindings))

      (princ "== Evaluating key bindings: DONE ==")
      ;; (when (> (length missing-bindings) 0)
      ;;   (message "Some of your commands are unbound."))
      )))

(defun loader-safe-module-list (loaded)
  "Write the list of loaded modules to `loader-module-file-name'."
  (let* ((lfn (file-truename loader-module-file-name)))
    (with-temp-buffer
      (insert (format "%S" loaded))
      (goto-char (point-min))
      (replace-string " " "\n  ")
      (write-file lfn nil))))

(defun loader-add-module ()
  "Add a module from `loader-user-lisp-directory' to `loader-module-file-name'.
Provides completion from all the basenames sans extensions found in
`loader-user-lisp-directory' but not (yet) in `loader-module-file-name'."
  (interactive)
  (let* ((lfn (file-truename loader-module-file-name))
         (loaded (read (file-string lfn)))
         (availables (cl-remove-if
                    (lambda (x)
                      (or (member (intern x) loaded)
                          (string-prefix-p "loader" x)))
                    (cl-remove-if
                     (lambda (x) (string-prefix-p "." x))
                     (mapcar
                      (lambda (x)
                        (file-name-sans-extension x))
                      (file-name-all-completions
                       loader-user-lisp-filename-prefix
                       loader-user-lisp-directory)))))
         (mod (completing-read "Add to loader modules: "
                               availables nil 'confirm)))
    (when (not (null mod))
      (add-to-list 'loaded (intern mod) t)
      (loader-safe-module-list loaded))))

(defun loader-remove-module ()
  "Remove a module from `loader-module-file-name'."
  (interactive)
  (let* ((lfn (file-truename loader-module-file-name))
         (loaded (read (file-string lfn)))
         (mod (completing-read "Remove from loader modules: " loaded nil t)))
    (when (not (null mod))
      (delete (intern mod) loaded)
      (loader-safe-module-list loaded))))

(defun loader-find-file ()
  "Find a module from `loader-user-lisp-directory' for editing.
Provides completion from all the basenames sans extensions found in
`loader-user-lisp-directory'."
  (interactive)
  (let* ((lfn (file-truename loader-module-file-name))
         (availables (file-name-all-completions
                      loader-user-lisp-filename-prefix
                      loader-user-lisp-directory))
         (mod (completing-read "Find file: "
                               availables nil 'confirm)))
    (when (not (null mod))
      (find-file
       (concat (file-name-as-directory loader-user-lisp-directory) mod)))))



;;; Safe handling of file and directory variables

(defun loader--user-lisp-directory ( &optional create )
  "Return the directory filename of the user's custom lisp directory.
If the directory does not exist, ask and evtl. create it, unless
CREATE is t in which case the directory is created, no questions
asked."
  (when (not (file-directory-p loader-user-lisp-directory))
    (if (or create
            (y-or-n-p (format
                       "loader-user-lisp-directory %s does not exist. Create it?"
                       loader-user-lisp-directory)))
        (make-directory loader-user-lisp-directory t)
      (error "Refused to create directory %s" loader-user-lisp-directory)))
  loader-user-lisp-directory)

(defun loader--package-list-file-name ()
  "Return the filename of the package list."
  (concat (loader--user-lisp-directory) "/loader-package.lst"))



;;; Write and load lists of m?elpa packages

(defun loader-store-package-list ( &optional force-overwrite )
  "Store the list of activated packages in loader-package.lst.
The file will reside in `loader-user-lisp-directory'."
  (interactive )
  (let* ((package-file (loader--package-list-file-name))
         (keep (find-buffer-visiting package-file))
         pbuff)

    (when (and (file-exists-p package-file)
               (not force-overwrite))
      (when (not (y-or-n-p (format "%s already exists! Overwrite?" package-file)))
        (error "Refused to overwrite %s" package-file)))

    (setq pbuff(find-file-noselect package-file))
    (with-current-buffer pbuff
      (erase-buffer)
      (insert ";; -*- mode: emacs-lisp -*-\n;;\n")
      (insert ";; This file is automatically (re-)created by loader-store-package-list\n;;\n")
      (insert "(progn\n")
      (dolist (package package-selected-packages)
        (insert (format "  (package-install '%s)\n" package)))
      (insert ")")
      (condition-case nil (save-buffer))
      (or keep (kill-buffer pbuff)))))

(defun loader-install-from-package-list ()
  "Try to install packages found in package list.
Error, if the list of packages does not exist."
  (interactive)
  (let* ((package-file (loader-package-list-file-name))
         (keep (find-buffer-visiting package-file))
         pbuff)

    (or (file-exists-p package-file)
        (error "Package list not found: %s" package-file))

    (setq pbuff (find-file-noselect package-file))
    (with-current-buffer pbuff
      (eval-buffer))))



(provide 'loader)
